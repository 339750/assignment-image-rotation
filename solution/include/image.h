//
// Created by Руслан Мартынов on 23.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H


#include <stdint.h>
#pragma pack(push, 2)
struct pixel
{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};
#pragma pack(pop)

struct image
{
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

void image_destroy(struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
