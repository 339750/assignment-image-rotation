//
// Created by Руслан Мартынов on 23.10.2022.
//


#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_IMAGE_H
#include "image.h"

struct image rotate(const struct image *image);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_IMAGE_H
