//
// Created by Руслан Мартынов on 23.10.2022.
//
#include <stdint.h>

#include <stdbool.h>
#include <stdio.h>

#include "image.h"

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

typedef struct bmp_header bmp_header;

//static int32_t padding_size(const struct image *img);
struct bmp_header bmp_header_create(const struct image *img);

enum read_status {
    READ_OK = 0,
    READ_INVALID_COMPRESSION,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_BITS_PER_PIXEL,
    READ_INVALID_FILE,
    IMAGE_INVALID_FILE
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status {
    WRITE_OK = 0,
    WRITE_DATA_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_INVALID_FILE,
    IMAGE_INVALID
};

enum write_status to_bmp(FILE *out, struct image const *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
