//
// Created by Руслан Мартынов on 25.10.2022.
//

#include "../include/image.h"
#include <stdlib.h>

void image_destroy(struct image img) {
    free(img . data);
}
