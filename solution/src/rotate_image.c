//
// Created by Руслан Мартынов on 23.10.2022.
//

#include "../include/rotate_image.h"
#include <stdlib.h>

struct image rotate(const struct image *image) {
    const uint64_t width = image->width;
    const uint64_t height = image->height;
    struct image rotated_image = {
            .width = height,
            .height = width,

    };
    struct pixel *data = malloc(sizeof(struct pixel) * width * height);
    for (size_t i = 0; i < height; ++i) {
        for (size_t j = 0; j < width; ++j) {
            data[height * j + (height - 1 - i)] = image->data[width * i + j];
        }
    }
    rotated_image.data = data;
    return rotated_image;

}

