#include "rotate_image.h"
#include "bmp.h"
#include <stdlib.h>

int main( int argc, char** argv ) {

    if (argc != 3) {
        fprintf(stderr, "Error: ./image-transformer <PathToBmpFile> <PathWhereToSaveBmp>\n");
        return 1;
    }

    FILE *in = fopen(argv[1], "rb");

    // read the input file
    if (in == NULL) {
        fprintf(stderr, "Error: Can't read file %s\n", argv[1]);
        return 1;
    }

    struct image img;

    enum read_status read_status = from_bmp(in, &img);
    if (read_status != READ_OK) {
        fprintf(stderr, "Error: Can't read file %s\n", argv[1]);
        return 1;
    }
    fprintf(stdout, "Image read successfully\n");

    // rotate the image
    fprintf(stdout, "Rotating image...\n");
    struct image rotated = rotate(&img);
    image_destroy(img);
    fprintf(stdout, "Rotated image");

    // write the output file
    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        fprintf(stderr, "Error: Can't write file %s\n", argv[2]);
        return 1;
    }
    fprintf(stdout, "Writing image...\n");

    enum write_status write_status = to_bmp(out, &rotated);
    if (write_status != WRITE_OK) {
        fprintf(stderr, "Error: Can't write file %s\n", argv[2]);
        return 1;
    }
    image_destroy(rotated);

    fclose(in);
    fclose(out);

    return 0;
}
