//
// Created by Руслан Мартынов on 25.10.2022.
//
#include "../include/bmp.h"
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static int32_t padding_size( const struct image *img )
{
    return ( (int32_t) (img -> width % 4));
}

struct bmp_header bmp_header_create(const struct image *img)
{
    uint64_t height = img -> height;
    uint64_t width = img -> width;
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = (sizeof(struct bmp_header) + height * width * sizeof(struct pixel)
                          + height * padding_size(img)),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = height * width * sizeof(struct pixel) + padding_size(img) * height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0

    };
    return header;
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    fread(header, sizeof(struct bmp_header), 1, in);
    if (header -> bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header -> biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    if (header -> biCompression != 0) {
        return READ_INVALID_COMPRESSION;
    }
    if (header->biSize != 40) {
        return READ_INVALID_HEADER;
    }

    if (header -> biHeight > 0 && header -> biWidth > 0 ) {
        *img = (struct image) {
                .width = header -> biWidth,.height = header -> biHeight,.data = malloc(sizeof(struct pixel) * header -> biWidth * header -> biHeight)
        };
    } else {
        return READ_INVALID_BITS;
    }

    for (uint32_t i = 0; i < header -> biHeight; ++i) {
        if (img == NULL) {
            return IMAGE_INVALID_FILE;
        } else {
            fread(&(img->data[i * img->width]), sizeof(struct pixel), header->biWidth, in);
            fseek(in, padding_size(img), SEEK_CUR);
        }
    }
    free(header);
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img)
{
    if (img == NULL) {
        return IMAGE_INVALID;
    } else {
        struct bmp_header header = bmp_header_create(img);
        fwrite(&header, sizeof(struct bmp_header), 1, out);
        for (uint32_t i = 0; i < img -> height; ++i) {
            fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out);
            const size_t padding_s = 0;
            fwrite(&padding_s, 1, padding_size(img), out);
        }
    }
    return WRITE_OK;
}
